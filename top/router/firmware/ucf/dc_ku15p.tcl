delete_pblocks quad_R0
delete_pblocks payload

create_pblock payload
resize_pblock payload -add SLICE_X19Y180:SLICE_X83Y659
set_property gridtypes {URAM288 RAMB36 RAMB18 DSP48E2 SLICE} [get_pblocks payload]

add_cells_to_pblock payload [get_cells payload]
