library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.ipbus.all;
use work.ipbus_reg_types.all;

use work.emp_data_types.all;
use work.emp_project_decl.all;
use work.emp_device_decl.all;
use work.emp_ttc_decl.all;

use work.ipbus_decode_emp_payload.all;

use work.dtc_link_maps.all;


entity emp_payload is
    port(
        --- Input Ports ---
        clk_p          : in  std_logic;
        clk40          : in  std_logic := '0';
        clk_payload    : in  std_logic_vector(2 downto 0);
        rst_payload    : in  std_logic_vector(2 downto 0);
        rst_loc        : in  std_logic_vector(N_REGION - 1 downto 0);
        clken_loc      : in  std_logic_vector(N_REGION - 1 downto 0);
        ctrs           : in  ttc_stuff_array;
        d              : in  ldata(4 * N_REGION - 1 downto 0);
        --- Output Ports ---
        bc0            : out std_logic;
        gpio           : out std_logic_vector(29 downto 0);
        gpio_en        : out std_logic_vector(29 downto 0);
        q              : out ldata(4 * N_REGION - 1 downto 0);
        --- IPBus Ports ---
        clk            : in  std_logic;
        rst            : in  std_logic;
        ipb_in         : in  ipb_wbus;
        ipb_out        : out ipb_rbus
    );
end emp_payload;

architecture rtl of emp_payload is


    -- IPBus fabric

    signal ipb_to_slaves           : ipb_wbus_array(N_SLAVES - 1 downto 0);
    signal ipb_from_slaves         : ipb_rbus_array(N_SLAVES - 1 downto 0);

    signal channel_sel             : ipb_reg_v(0 downto 0);


    -- FE data extraction and monitoring

    signal ipb_chain               : ipbdc_bus_array(cNumberOfFEModules downto 0);
    signal stubs                   : ldata(cNumberOfFEModules - 1 downto 0);


    -- BE Stub Processing and Routing

    signal router_output : ldata(cNumberOfOutputLinks - 1 downto 0) := (others => LWORD_NULL);

begin


    --==============================--
    -- IPBus fabric
    --==============================--


    --==============================--
    fabric: entity work.ipbus_fabric_sel
    --==============================--
    generic map(
        NSLV            => N_SLAVES,
        SEL_WIDTH       => IPBUS_SEL_WIDTH
    )
    port map(
        ipb_in          => ipb_in,
        ipb_out         => ipb_out,
        sel             => ipbus_sel_emp_payload(ipb_in.ipb_addr),
        ipb_to_slaves   => ipb_to_slaves,
        ipb_from_slaves => ipb_from_slaves
    );


    --==============================--
    channel_ctrl: entity work.ipbus_reg_v
    --==============================--
    generic map(
        N_REG           => 1
    )
    port map(
        clk             => clk,
        reset           => rst,
        ipbus_in        => ipb_to_slaves(N_SLV_FE_CTRL),
        ipbus_out       => ipb_from_slaves(N_SLV_FE_CTRL),
        q               => channel_sel,
        qmask           => (0 => X"0000007f")
    );


    --==============================--
    channel_select: entity work.ipbus_dc_fabric_sel
    --==============================--
    generic map(
        SEL_WIDTH       => 7
    )
    port map(
        clk             => clk,
        rst             => rst,
        sel             => channel_sel(0)(6 downto 0),
        ipb_in          => ipb_to_slaves(N_SLV_FE_CHAN),
        ipb_out         => ipb_from_slaves(N_SLV_FE_CHAN),
        ipbdc_out       => ipb_chain(0),
        ipbdc_in        => ipb_chain(cNumberOfFEModules)
    );



    --==============================--
    -- FE data extraction and monitoring
    --==============================--


    --==============================--
    genGBTExtractor: for i in 0 to cNumberOfFEModules - 1 generate
    --==============================--

        signal ipb_to_channel   : ipb_wbus;
        signal ipb_from_channel : ipb_rbus;

    begin

        --==============================--
        channel_node: entity work.ipbus_dc_node
        --==============================--
        generic map(
            I_SLV       => i,
            SEL_WIDTH   => 7,
            PIPELINE    => false
        )
        port map(
            clk         => clk,
            rst         => rst,
            ipb_out     => ipb_to_channel,
            ipb_in      => ipb_from_channel,
            ipbdc_in    => ipb_chain(i),
            ipbdc_out   => ipb_chain(i + 1)
        );

        --==============================--
    	LinkInterfaceInstance: entity work.LinkInterface
        --==============================--
    	port map(
            --- Input Ports ---
            clk_p       => clk_p,
            link_in     => d(cDTCInputLinkMap(i)),
            --- Output Ports ---
            link_out    => q(cDTCInputLinkMap(i)),
            stub_out    => stubs(i),
            --- IPBus Ports ---
            clk         => clk,
            rst         => rst,
            ipb_in      => ipb_to_channel,
            ipb_out     => ipb_from_channel
    	);

    end generate;


    --==============================--
    -- BE Stup Formatting and Routing
    --==============================--

    --==============================--
    RouterInstance: entity work.DummyRouter
    --==============================--
    port map(
        --- Input Ports ---
        clk_p    => clk_p,
        dtc_din  => stubs,
        --- Output Ports ---
        dtc_dout => router_output
    );

    genOutputLinks: for i in 0 to cNumberOfOutputLinks - 1 generate
    begin
        q(cDTCOutputLinkMap(i)) <= router_output(i);
    end generate;




    bc0     <= '0';
    gpio    <= (others => '0');
    gpio_en <= (others => '0');



end rtl;
