library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

package dtc_link_maps is
    constant cNumberOfFEModules : integer := 1;
    constant cNumberOfOutputLinks : integer := 18;

    type tDTCInputLinkMap is array(0 to cNumberOfFEModules - 1) of integer;
    constant cDTCInputLinkMap : tDTCInputLinkMap := (0 => 4);

    type tDTCOutputLinkMap is array(0 to cNumberOfOutputLinks - 1) of integer;
    -- constant cDTCOutputLinkMap : tDTCOutputLinkMap := (40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57);
    constant cDTCOutputLinkMap : tDTCOutputLinkMap := (1, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57);
end package dtc_link_maps;
