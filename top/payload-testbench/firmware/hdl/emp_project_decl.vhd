-- emp_project_decl
--
-- Defines constants for the whole device

library ieee;
use ieee.std_logic_1164.all;

use work.emp_framework_decl.all;
use work.emp_device_types.all;

-------------------------------------------------------------------------------
package emp_project_decl is

  constant PAYLOAD_REV         : std_logic_vector(31 downto 0) := X"D7C00001";

  -- Latency buffer size
  constant LB_ADDR_WIDTH      : integer             := 10;

  -- Clock setup
  constant CLOCK_COMMON_RATIO : integer               := 32;
  constant CLOCK_RATIO        : integer               := 8;
  constant CLOCK_AUX_DIV      : clock_divisor_array_t := (16, 8, 4); -- Dividers of CLOCK_COMMON_RATIO * 40 MHz

  -- Only used by nullalgo
  constant PAYLOAD_LATENCY    : integer             := 5;

  -- mgt -> buf -> fmt -> (algo) -> (fmt) -> buf -> mgt -> clk -> altclk
  constant REGION_CONF : region_conf_array_t := (
    0  => (no_mgt, buf, no_fmt, buf, no_mgt),      --Bank 225 -- Right Column
    1  => (no_mgt, buf, no_fmt, buf, no_mgt),      --Bank 226
    -- 2  => (no_mgt, buf, no_fmt, buf, no_mgt),   --Bank 227
    -- 3  => (no_mgt, buf, no_fmt, buf, no_mgt),   --Bank 228
    -- 4  => (no_mgt, buf, no_fmt, buf, no_mgt),   --Bank 229
    -- 5  => (no_mgt, buf, no_fmt, buf, no_mgt),   --Bank 230
    -- 6  => (no_mgt, buf, no_fmt, buf, no_mgt),   --Bank 231
    -- 7  => (no_mgt, buf, no_fmt, buf, no_mgt),   --Bank 232
    -- 8  => (no_mgt, buf, no_fmt, buf, no_mgt),   --Bank 233
    -- 9  => (no_mgt, buf, no_fmt, buf, no_mgt),   --Bank 234
    -- -- Cross-chip
    -- 10 => (no_mgt, buf, no_fmt, buf, no_mgt),   --Bank 134 -- Left Column
    -- 11 => (no_mgt, buf, no_fmt, buf, no_mgt),   --Bank 133
    -- 12 => (no_mgt, buf, no_fmt, buf, no_mgt),   --Bank 132
    -- 13 => (no_mgt, buf, no_fmt, buf, no_mgt),   --Bank 131
    -- 14 => (no_mgt, buf, no_fmt, buf, no_mgt),   --Bank 130
    -- 15 => (no_mgt, buf, no_fmt, buf, no_mgt),   --Bank 129
    -- 16 => (no_mgt, buf, no_fmt, buf, no_mgt),   --Bank 128
    -- 17 => (no_mgt, buf, no_fmt, buf, no_mgt),   --Bank 127
    others => kDummyRegion
  );

end emp_project_decl;
-------------------------------------------------------------------------------
