-- emp_project_decl
--
-- Defines constants for the whole device

library ieee;
use ieee.std_logic_1164.all;


library lpgbt_lib;
use lpgbt_lib.lpgbtfpga_package.all;
use work.emp_lpgbt_decl.all;

use work.emp_framework_decl.all;
use work.emp_device_decl.all;
use work.emp_device_types.all;

use work.emp_gbt_package.all;
use work.emp_data_framer_decl.all;
use work.gbt_bank_package.all;

-------------------------------------------------------------------------------
package emp_project_decl is

  constant PAYLOAD_REV         : std_logic_vector(31 downto 0) := X"D7C00002";

  -- Latency buffer size
  constant LB_ADDR_WIDTH      : integer             := 10;

  -- Clock setup
  constant CLOCK_COMMON_RATIO : integer               := 32;
  constant CLOCK_RATIO        : integer               := 8;
  constant CLOCK_AUX_DIV      : clock_divisor_array_t := (16, 8, 4); -- Dividers of CLOCK_COMMON_RATIO * 40 MHz

  -- Only used by nullalgo
  constant PAYLOAD_LATENCY    : integer             := 5;

  -- mgt -> buf -> fmt -> (algo) -> (fmt) -> buf -> mgt -> clk -> altclk
  constant REGION_CONF : region_conf_array_t := (
--    0  => (lpgbt, buf, no_fmt, buf, lpgbt),   --Bank 225 -- Right Column
--    1  => (lpgbt, buf, no_fmt, buf, lpgbt),   --Bank 226
    2  => (lpgbt, buf, no_fmt, buf, lpgbt),   --Bank 227
    3  => (no_mgt, buf, no_fmt, buf, no_mgt),   --Bank 228
--    4  => (lpgbt, buf, no_fmt, buf, lpgbt),   --Bank 229
    5  => (no_mgt, buf, no_fmt, buf, no_mgt),   --Bank 230
--    6  => (lpgbt, buf, no_fmt, buf, lpgbt),   --Bank 231
--    7  => (lpgbt, buf, no_fmt, buf, lpgbt),   --Bank 232
--    8  => (lpgbt, buf, no_fmt, buf, lpgbt),   --Bank 233
--    9  => (lpgbt, buf, no_fmt, buf, lpgbt),   --Bank 234
--    -- Cross-chip
--    10 => (gty25, buf, no_fmt, buf, gty25),   --Bank 134 -- Left Column
--    11 => (gty25, buf, no_fmt, buf, gty25),   --Bank 133
--    12 => (gty25, buf, no_fmt, buf, gty25),   --Bank 132
--    13 => (gty25, buf, no_fmt, buf, gty25),   --Bank 131
--    14 => (gty25, buf, no_fmt, buf, gty25),   --Bank 130
--    15 => (gty25, buf, no_fmt, buf, gty25),   --Bank 129
--    16 => (gty25, buf, no_fmt, buf, gty25),   --Bank 128
--    17 => (gty25, buf, no_fmt, buf, gty25),   --Bank 127
    others => kDummyRegion
  );

  -- for data framer (ic_simple, no_ec, n_ec_spare, ec_broadcast)
  constant REGION_DATA_FRAMER_CONF : region_data_framer_conf_array_t := (
    2 => ( 0=>(false, true, 0, false), 1=>(false, true, 0, false), 2=>(false, true, 0, false), 3=>(false, true, 0, false)),
    others => kDummyRegionDataFramer
  );

  -- for gbt
  constant REGION_GBT_CONF : region_gbt_conf_array_t := (
      -- 1 => ( STANDARD, STANDARD, WIDE_BUS, WIDE_BUS),
      others => kDummyRegionGbt
  );

  -- for lpgbt
  constant REGION_LPGBT_CONF : region_lpgbt_conf_array_t := (
    2  => (FEC5, DATARATE_5G12, PCS),
    others => kDummyRegionLpgbt
  );



end emp_project_decl;
-------------------------------------------------------------------------------
