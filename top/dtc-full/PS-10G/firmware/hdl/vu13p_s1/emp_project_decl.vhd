-- emp_project_decl
--
-- Defines constants for the whole device

library ieee;
use ieee.std_logic_1164.all;

library lpgbt_lib;
use lpgbt_lib.lpgbtfpga_package.all;

use work.emp_framework_decl.all;
use work.emp_device_decl.all;
use work.emp_device_types.all;
use work.emp_lpgbt_decl.all;
use work.emp_data_framer_decl.all;

-------------------------------------------------------------------------------
package emp_project_decl is

  constant PAYLOAD_REV         : std_logic_vector(31 downto 0) := X"D7C00002";

  -- Latency buffer size
  constant LB_ADDR_WIDTH      : integer               := 10;

  -- Clock setup
  constant CLOCK_COMMON_RATIO : integer               := 32;
  constant CLOCK_RATIO        : integer               := 8;
  constant CLOCK_AUX_DIV      : clock_divisor_array_t := (16, 9, 4); -- Dividers of CLOCK_COMMON_RATIO * 40 MHz

  -- Only used by nullalgo
  constant PAYLOAD_LATENCY    : integer             := 5;

  -- mgt -> buf -> fmt -> (algo) -> (fmt) -> buf -> mgt -> clk -> altclk
  constant REGION_CONF : region_conf_array_t := (
    0      => kDummyRegion,                      -- PCIe, AXI & TCDS
    1      => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-01
    2      => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-01
    3      => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-01
    4      => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-02
    5      => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-02
    6      => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-02
    7      => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-03
    8      => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-03
    9      => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-03
    10     => (gty25, buf, no_fmt, buf, gty25),  -- ff-04
    11     => (gty25, buf, no_fmt, buf, gty25),  -- ff-04
    12     => (gty25, buf, no_fmt, buf, gty25),  -- ff-04
    13     => (gty25, buf, no_fmt, buf, gty25),  -- ff-05
    14     => (gty25, buf, no_fmt, buf, gty25),  -- ff-05
    15     => (gty25, buf, no_fmt, buf, gty25),  -- ff-05
    -- Cross-chip
    16     => (gty25, buf, no_fmt, buf, gty25),  -- ff-06
    17     => (gty25, buf, no_fmt, buf, gty25),  -- ff-06
    18     => (gty25, buf, no_fmt, buf, gty25),  -- ff-06
    19     => (gty25, buf, no_fmt, buf, gty25),  -- ff-07
    20     => (gty25, buf, no_fmt, buf, gty25),  -- ff-07
    21     => (gty25, buf, no_fmt, buf, gty25),  -- ff-07
    22     => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-08
    23     => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-08
    24     => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-08
    25     => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-09
    26     => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-09
    27     => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-09
    28     => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-10
    29     => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-10
    30     => (lpgbt, buf, no_fmt, buf, lpgbt),  -- ff-10
    31     => (gty25, buf, no_fmt, buf, gty25),  -- ff-bi
    others => kDummyRegion
  );

  -- for data framer (ic_simple, no_ec, n_ec_spare, ec_broadcast)
  constant REGION_DATA_FRAMER_CONF : region_data_framer_conf_array_t := (
    1  => ( 0=>(false, true, 0, false), 1=>(false, true, 0, false), 2=>(false, true, 0, false), 3=>(false, true, 0, false)),
    2  => ( 0=>(false, true, 0, false), 1=>(false, true, 0, false), 2=>(false, true, 0, false), 3=>(false, true, 0, false)),
    3  => ( 0=>(false, true, 0, false), 1=>(false, true, 0, false), 2=>(false, true, 0, false), 3=>(false, true, 0, false)),
    4  => ( 0=>(false, true, 0, false), 1=>(false, true, 0, false), 2=>(false, true, 0, false), 3=>(false, true, 0, false)),
    5  => ( 0=>(false, true, 0, false), 1=>(false, true, 0, false), 2=>(false, true, 0, false), 3=>(false, true, 0, false)),
    6  => ( 0=>(false, true, 0, false), 1=>(false, true, 0, false), 2=>(false, true, 0, false), 3=>(false, true, 0, false)),
    7  => ( 0=>(false, true, 0, false), 1=>(false, true, 0, false), 2=>(false, true, 0, false), 3=>(false, true, 0, false)),
    8  => ( 0=>(false, true, 0, false), 1=>(false, true, 0, false), 2=>(false, true, 0, false), 3=>(false, true, 0, false)),
    9  => ( 0=>(false, true, 0, false), 1=>(false, true, 0, false), 2=>(false, true, 0, false), 3=>(false, true, 0, false)),
    22 => ( 0=>(false, true, 0, false), 1=>(false, true, 0, false), 2=>(false, true, 0, false), 3=>(false, true, 0, false)),
    23 => ( 0=>(false, true, 0, false), 1=>(false, true, 0, false), 2=>(false, true, 0, false), 3=>(false, true, 0, false)),
    24 => ( 0=>(false, true, 0, false), 1=>(false, true, 0, false), 2=>(false, true, 0, false), 3=>(false, true, 0, false)),
    25 => ( 0=>(false, true, 0, false), 1=>(false, true, 0, false), 2=>(false, true, 0, false), 3=>(false, true, 0, false)),
    26 => ( 0=>(false, true, 0, false), 1=>(false, true, 0, false), 2=>(false, true, 0, false), 3=>(false, true, 0, false)),
    27 => ( 0=>(false, true, 0, false), 1=>(false, true, 0, false), 2=>(false, true, 0, false), 3=>(false, true, 0, false)),
    28 => ( 0=>(false, true, 0, false), 1=>(false, true, 0, false), 2=>(false, true, 0, false), 3=>(false, true, 0, false)),
    29 => ( 0=>(false, true, 0, false), 1=>(false, true, 0, false), 2=>(false, true, 0, false), 3=>(false, true, 0, false)),
    30 => ( 0=>(false, true, 0, false), 1=>(false, true, 0, false), 2=>(false, true, 0, false), 3=>(false, true, 0, false)), 
    others => kDummyRegionDataFramer
  );

  -- for lpgbt
  constant REGION_LPGBT_CONF : region_lpgbt_conf_array_t := (
    1  => (FEC5, DATARATE_10G24, PCS),
    2  => (FEC5, DATARATE_10G24, PCS),    
    3  => (FEC5, DATARATE_10G24, PCS),
    4  => (FEC5, DATARATE_10G24, PCS),
    5  => (FEC5, DATARATE_10G24, PCS),
    6  => (FEC5, DATARATE_10G24, PCS),    
    7  => (FEC5, DATARATE_10G24, PCS),
    8  => (FEC5, DATARATE_10G24, PCS),
    9  => (FEC5, DATARATE_10G24, PCS),
    22 => (FEC5, DATARATE_10G24, PCS),    
    23 => (FEC5, DATARATE_10G24, PCS),
    24 => (FEC5, DATARATE_10G24, PCS),
    25 => (FEC5, DATARATE_10G24, PCS),
    26 => (FEC5, DATARATE_10G24, PCS),    
    27 => (FEC5, DATARATE_10G24, PCS),
    28 => (FEC5, DATARATE_10G24, PCS),
    29 => (FEC5, DATARATE_10G24, PCS),
    30 => (FEC5, DATARATE_10G24, PCS),
    others => kDummyRegionLpgbt
  );



end emp_project_decl;
-------------------------------------------------------------------------------
