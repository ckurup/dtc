library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

package dtc_link_maps is
    constant cNumberOfFEModules   : integer := 72;
    constant cNumberOfOutputLinks : integer := 36;

    type tDTCInputLinkMap is array(0 to cNumberOfFEModules - 1) of integer;
    constant cDTCInputLinkMap : tDTCInputLinkMap := (
  4,   5,   6,   7,   8,   9,  10,  11,  12,  13,  14,  15,
 16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  26,  27,
 28,  29,  30,  31,  32,  33,  34,  35,  36,  37,  38,  39,
 88,  89,  90,  91,  92,  93,  94,  95,  96,  97,  98,  99,
100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111,
112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123
    );

    type tDTCOutputLinkMap is array(0 to cNumberOfOutputLinks - 1) of integer;
    constant cDTCOutputLinkMap : tDTCOutputLinkMap := (
46,  47,  48,  49,  50,  51,  52,  53,  54,
55,  56,  57,  58,  59,  60,  61,  62,  63,
64,  65,  66,  67,  68,  69,  70,  71,  72,
73,  74,  75,  76,  77,  78,  79,  80,  81
    );

end package dtc_link_maps;
