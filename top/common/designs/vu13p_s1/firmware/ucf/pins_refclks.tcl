
# MGT REF CLOCKS - ASYNC (U0:3, U1:3)

# Bank N, 222 Refclk1
set_property PACKAGE_PIN AW11 [get_ports {refclkp[0]}]
# Bank P, 224 Refclk1
set_property PACKAGE_PIN AR11 [get_ports {refclkp[1]}]
# Bank B, 226 Refclk1
set_property PACKAGE_PIN AL11 [get_ports {refclkp[2]}]
# Bank D, 228 Refclk1
set_property PACKAGE_PIN AC11 [get_ports {refclkp[3]}]
# Bank F, 230 Refclk1
set_property PACKAGE_PIN V13 [get_ports {refclkp[4]}]
# Bank H, 232 Refclk1
set_property PACKAGE_PIN P13 [get_ports {refclkp[5]}]
# Bank J, 234 Refclk1
set_property PACKAGE_PIN K13 [get_ports {refclkp[6]}]

# Bank Z, 134 Refclk1
set_property PACKAGE_PIN K39 [get_ports {refclkp[7]}]
# Bank X, 132 Refclk1
set_property PACKAGE_PIN P39 [get_ports {refclkp[8]}]
# Bank V, 130 Refclk1
set_property PACKAGE_PIN V39 [get_ports {refclkp[9]}]
# Bank T, 128 Refclk1
set_property PACKAGE_PIN AC41 [get_ports {refclkp[10]}]
# Bank R, 126 Refclk1
set_property PACKAGE_PIN AL41 [get_ports {refclkp[11]}]
# Bank F, 124 Refclk1
set_property PACKAGE_PIN AR41 [get_ports {refclkp[12]}]
# Bank AD, 122 Refclk1
set_property PACKAGE_PIN AW41 [get_ports {refclkp[13]}]
# Bank AB, 120 Refclk1
set_property PACKAGE_PIN BC41 [get_ports {refclkp[14]}]


# MGT REF CLOCKS - SYNC  (U0:2, U1:2)

# Bank N, 222 Refclk0
set_property PACKAGE_PIN AY13 [get_ports {refclkp[15]}]
# Bank P, 224 Refclk0
set_property PACKAGE_PIN AT13 [get_ports {refclkp[16]}]
# Bank B, 226 Refclk0
set_property PACKAGE_PIN AM13 [get_ports {refclkp[17]}]
# Bank D, 228 Refclk0
set_property PACKAGE_PIN AE11 [get_ports {refclkp[18]}]
# Bank F, 230 Refclk0
set_property PACKAGE_PIN W11 [get_ports {refclkp[19]}]
# Bank H, 232 Refclk0
set_property PACKAGE_PIN R11 [get_ports {refclkp[20]}]
# Bank J, 234 Refclk0
set_property PACKAGE_PIN L11 [get_ports {refclkp[21]}]

# Bank Z, 134 Refclk0
set_property PACKAGE_PIN L41 [get_ports {refclkp[22]}]
# Bank X, 132 Refclk0
set_property PACKAGE_PIN R41 [get_ports {refclkp[23]}]
# Bank V, 130 Refclk0
set_property PACKAGE_PIN W41 [get_ports {refclkp[24]}]
# Bank T, 128 Refclk0
set_property PACKAGE_PIN AE41 [get_ports {refclkp[25]}]
# Bank R, 126 Refclk0
set_property PACKAGE_PIN AM39 [get_ports {refclkp[26]}]
# Bank F, 124 Refclk0
set_property PACKAGE_PIN AT39 [get_ports {refclkp[27]}]
# Bank AD, 122 Refclk0
set_property PACKAGE_PIN AY39 [get_ports {refclkp[28]}]
# Bank AB, 120 Refclk0
set_property PACKAGE_PIN BD39 [get_ports {refclkp[29]}]
