import numpy as np
from typing import List, Generator
import datapath
import framer
from collections import Counter
from glob import glob
from matplotlib import pyplot as plt


def convertBinaryArrayToInt(array):
    array_str: str = "".join(str(i) for i in array)
    value: int = int(array_str, base=2)
    return value


def headerAligner(stream: np.ndarray) -> Generator[int, None, None]:
    window_size = 68
    for i in range(window_size, len(stream)):
        bcid: int = convertBinaryArrayToInt(np.concatenate((stream[i-1], stream[i])))
        previous_bcid: int = convertBinaryArrayToInt(np.concatenate((stream[i-65], stream[i-64])))
        if bcid == previous_bcid + 2 and np.all(stream[i-4][-4:] == 0) and np.all(stream[i-68][-4:] == 0):
            yield (i - 67)


class Boxcar:
    boxcar_frames = 64

    header_lengths = {
        'fe_type': 1,
        'status': 9,
        'bcid': 12,
        'no_stubs': 6
    }

    def __init__(self, bitstream: np.ndarray):
        self.bitstream: np.ndarray = bitstream
        self.header: Dict = {}
        self.stubs: List = []
        self.extractHeader()
        self.extractStubs()

    def extractHeader(self):
        remaining_bits = 0
        bit_index = 0
        frame_index = 0
        for i in ['fe_type', 'status', 'bcid', 'no_stubs']:
            if self.number_of_links - bit_index >= self.header_lengths[i]:
                header_array = self.bitstream[frame_index][bit_index:bit_index+self.header_lengths[i]]
                bit_index += self.header_lengths[i]
            else:
                header_array = np.empty(0, dtype=int)
                remaining_bits = self.header_lengths[i]
                while remaining_bits > 0:
                    if remaining_bits > self.number_of_links - bit_index:
                        header_array = np.append(header_array, self.bitstream[frame_index][bit_index:])
                        remaining_bits -= self.number_of_links - bit_index
                        bit_index = 0
                        frame_index += 1
                    else:
                        header_array = np.append(header_array, self.bitstream[frame_index][:remaining_bits])
                        bit_index = remaining_bits
                        remaining_bits = 0
            self.header[i] = "".join(str(i) for i in header_array) if len(header_array) > 1 else header_array[0]
        self.header['bcid_int'] = int(self.header['bcid'], 2)
        self.header['no_stubs_int'] = int(self.header['no_stubs'], 2)

    def extractStubs(self):
        stub_map = self.generateStubMap()
        stubs_array = [np.zeros(self.stub_width, dtype=int) for i in range(self.max_stubs)]
        stubs_array[0][self.stub_width-self.stub_bits_in_header:] = self.bitstream[self.header_frames-1][self.number_of_links-self.stub_bits_in_header:][::-1]
        for i, frame in enumerate(stub_map):
            for j, pair in enumerate(frame):
                stubs_array[pair[0]][pair[1]] = self.bitstream[self.header_frames+i][self.number_of_links-1-j]
        stubs_array[-1][:self.stub_bits_in_footer] = self.bitstream[self.footer_index][:self.stub_bits_in_footer][::-1]
        self.stubs = ["".join(str(j) for j in i[::-1]) for i in stubs_array][:self.header['no_stubs_int']]

    def generateStubMap(self):
        stub_index = 0
        # map = []
        remaining_bits = self.stub_width - self.stub_bits_in_header - 1
        for frame in range(self.boxcar_frames - self.header_frames):
            frame_map = []
            for link in range(self.number_of_links - 1, -1, -1):
                if remaining_bits >= 0:
                    frame_map.append((stub_index, remaining_bits))
                    remaining_bits -= 1
                else:
                    remaining_bits = self.stub_width - 1
                    stub_index += 1
                    if stub_index == self.max_stubs:
                        break
                    frame_map.append((stub_index, remaining_bits))
                    remaining_bits -= 1
            yield frame_map[::-1]
            if stub_index == self.max_stubs:
                break

    def generateStubReadyIndices(self):
        stub_map = self.generateStubMap()
        for i, frame in enumerate(stub_map):
            for j in frame:
                if j[1] == 0:
                    yield self.header_frames + i + 1


class BoxCar2S5GFEC12(Boxcar):
    stub_width = 18
    number_of_links = 5
    stub_bits_in_header = 2
    footer_index = 63
    stub_bits_in_footer = 1
    header_frames = 6
    max_stubs = 16


class BoxCar2S5GFEC5(Boxcar):
    stub_width = 18
    number_of_links = 6
    stub_bits_in_header = 2
    footer_index = 61
    stub_bits_in_footer = 4
    header_frames = 5
    max_stubs = 19


class BoxCarPS5GFEC5(Boxcar):
    stub_width = 21
    number_of_links = 6
    stub_bits_in_header = 2
    footer_index = 60
    stub_bits_in_footer = 4
    header_frames = 5
    max_stubs = 16


class BoxCarPS10GFEC5(Boxcar):
    stub_width = 21
    number_of_links = 12
    stub_bits_in_header = 8
    footer_index = 63
    stub_bits_in_footer = 7
    header_frames = 3
    max_stubs = 35
