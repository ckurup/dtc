import numpy as np


class BoxcarBuilder:
    def __init__(self, target):
        self.target = target

    def generateHeader(self, status: str = "000000000", bcid_int: int = 0, no_stubs_int: int = 0):
        assert no_stubs_int <= self.target.max_stubs, "Number of stubs cannot be greater than the maximum for the boxcar type."
        self.no_stubs = no_stubs_int
        self.header = {
            "fe_type": str(int("PS" in self.target.__name__)),
            "status": status,
            "bcid": "{:0{bit_length}b}".format(bcid_int, bit_length=self.target.header_lengths["bcid"]),
            "no_stubs": "{:0{bit_length}b}".format(no_stubs_int, bit_length=self.target.header_lengths["no_stubs"])
        }
        return self.header

    def generateStubs(self, pattern):
        assert hasattr(self, "header"), "BoxCarBuilder.generateHeader must be ran before stubs can be generated."
        assert len(pattern) == self.target.stub_width, "Pattern length must equal target stub width."
        self.stubs = [pattern for i in range(self.no_stubs)]
        return self.stubs

    def addStubs(self, stubs):
        assert hasattr(self, "header"), "BoxCarBuilder.generateHeader must be ran before stubs can be generated."
        assert len(stubs) == self.no_stubs, "Length of stub array must equal number of stubs specified in header."
        for i in stubs:
            assert len(i) == self.target.stub_width, "Stub length must equal target stub width."

    def build(self):
        assert hasattr(self, "header"), "Build function requires a header, run BoxCarBuilder.generateHeader before calling build."
        assert hasattr(self, "stubs"), "Build function requires stubs, run BoxCarBuilder.generateStubs or BoxCarBuilder.addStubs before calling build."
        stream = ""
        for i in ["fe_type", "status", "bcid", "no_stubs"]:
            stream += self.header[i]
        for i in self.stubs:
            stream += i
        while len(stream) < 64*self.target.number_of_links:
            stream += "0"
        stream_array = map("".join, zip(*[iter(stream)]*self.target.number_of_links))
        return np.array([[int(j, 2) for j in list(i)] for i in stream_array])
