import numpy as np
import pandas as pd
from glob import glob
from matplotlib import pyplot as plt


def plotMeanBitValues(array, label, errors=True):
    if errors:
        plt.errorbar(
            x=np.arange(len(array[0])),
            y=np.mean(array, axis=0),
            yerr=np.var(array, axis=0),
            ls='none', capsize=2, label=label)
    else:
        plt.scatter(
            x=np.arange(len(array[0])),
            y=np.mean(array, axis=0),
            label=label
        )


def main():
    arr, arr_bad = [], []
    for filename in glob("data/good/rx_summary_header_*.csv"):
        arr.append(pd.read_csv(filename, delimiter=",", dtype=int).values)
    for filename in glob("data/bad/rx_summary_header_run_3_*.csv"):
        arr_bad.append(pd.read_csv(filename, delimiter=",", dtype=int).values)
    plotMeanBitValues(np.concatenate(arr), label="Good")
    plotMeanBitValues(np.concatenate(arr_bad), label="Bad")
    plt.xlabel("Bit Index")
    plt.ylabel("Mean bit value")
    plt.legend()
    plt.show()
    plt.scatter(
        x=np.arange(112),
        y=np.abs(np.mean(np.concatenate(arr), axis=0) - np.mean(np.concatenate(arr_bad), axis=0))
    )
    plt.yscale('log')
    plt.ylim(1e-6,1e0)
    plt.show()


if __name__ == '__main__':
    main()
