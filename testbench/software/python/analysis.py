import numpy as np
import pandas as pd
from glob import glob
from collections import Counter


class Boxcar:
    def __init__(self, bitstream):
        self.bitstream = bitstream
        self.header = {}
        self.header['fe_type'] = bitstream[0][0]
        self.header['status'] = np.concatenate(
            [bitstream[0][1:], bitstream[1]])
        self.header['bcid'] = np.concatenate(
            [bitstream[2], bitstream[3], bitstream[4][:2]])
        self.header['no_stubs'] = np.concatenate(
            [bitstream[4][2:], bitstream[5][:3]])


def loadPatternFile(filename):
    header_lines = 12
    slice_length = 56
    with open(filename) as f:
        lines = [line.strip() for line in f][header_lines:]
    frames = [line.split(":")[1].split(" ")[1][2:] for line in lines]
    words = ["{:0{slice_length}b}".format(int(frame, 16), slice_length=slice_length)[::-1] for frame in frames]
    return words


def combineWords(words):
    words_per_frame = 2
    l1a_slice = (40, 48)
    if words[0][l1a_slice[0]:l1a_slice[1]] == "01010101":
        offset = 0
        frame_count = int(len(words)/words_per_frame)
    else:
        offset = 1
        frame_count = int(len(words[1:])/words_per_frame)
    gbt_frames = []
    for i in range(frame_count):
        gbt_frames.append(words[i*words_per_frame + offset] + words[i*words_per_frame + offset + 1])
    return gbt_frames


def convertToCICFrames(gbt_frames):
    link_indices = ((96, 32, 80, 8, 88), (56, 104, 24, 72, 16))
    cic_frames = [[],[]]
    for frame in gbt_frames:
        for i, indices in enumerate(link_indices):
            bx = np.array([np.array(list(frame[index:index+8]), dtype=int) for index in indices]).T
            cic_frames[i].append(bx)

    return [np.concatenate(i) for i in cic_frames]


def getHeaderStartPoints(stream):
    window_size = 68
    header_indices = []
    for i in range(window_size, len(stream)):
        bcid = "".join(str(i) for i in np.concatenate((stream[i], stream[i-1])))
        bcid = int(bcid, 2)
        previous_bcid = "".join(str(i) for i in np.concatenate((stream[i-64], stream[i-65])))
        previous_bcid = int(previous_bcid, 2)
        if bcid == previous_bcid + 2 and np.all(stream[i-4][-4:] == 0) and np.all(stream[i-68][-4:] == 0):
            header_indices.append(i - 68)
    return header_indices


def main(filename):
    print("Analysing file: {}".format(filename))
    words = loadPatternFile(filename)
    gbt_frames = combineWords(words)
    print("WIDE_BUS frames in file: {}".format(len(gbt_frames)))
    cic_streams = convertToCICFrames(gbt_frames)
    for i, stream in enumerate(cic_streams):
        header_indices = getHeaderStartPoints(stream)
        print("Header gaps Counter:", Counter((np.diff(header_indices))))
        boxcars = [Boxcar(stream[j:j+64]) for j in header_indices]
        if i == 0:
            for j in boxcars:
                print(j.header)


if __name__ == '__main__':
    for filename in glob("data/rx_summary_empty*.txt"):
        main(filename)
