import numpy as np
from typing import List
from glob import glob
import pandas as pd

import datapath
import framer
from payload import headerAligner, BoxCar2S5GFEC12


class Capture:
    def __init__(self, filename):
        self.filename = filename
        self.gbt_frames = [i for i in datapath.generateGBTFrames(filename)]
        self.array = np.array(
            [[int(j) for j in list(i)] for i in self.gbt_frames])

    def checkQuality(self, cic_index=0):
        cic_streams: List[np.ndarray] = []
        for gbt_word in self.gbt_frames:
            cic_streams.append(framer.frame(gbt_word))
        cic_arrays: np.ndarray = np.concatenate(cic_streams, axis=1)  # Has shape (2, N, 5)
        cic_stream = cic_arrays[cic_index]
        header_indices: List[int] = [i for i in headerAligner(cic_stream)]
        if len(header_indices) < 60:
            self.quality = False
            return self.quality
        for index in header_indices:
            boxcar = BoxCar2S5GFEC12(cic_stream[index:index+64])
            if boxcar.header['no_stubs_int'] > 0:
                self.quality = False
                return self.quality
        self.quality = True
        return self.quality

    def getArray(self):
        return self.array

    def saveArray(self):
        if not hasattr(self, 'quality'):
            self.checkQuality()
        filename = "data/{}/{}.csv".format(
            "good" if self.quality else "bad",
            self.filename.split("/")[-1].split(".")[0]
        )
        # np.savetxt(filename, self.array, fmt="%d", delimiter=",")
        pd.DataFrame(self.array).to_csv(filename, header=None, index=None)


def main() -> None:
    good_count = 0
    for count, filename in enumerate(glob("data/original/rx_summary_reset_*.txt")):
        capture = Capture(filename)
        good_count += capture.checkQuality()
        capture.saveArray()
    print("Ingested {} captures.".format(count + 1))
    print("Quality fraction: {}%".format(good_count*100/(count+1)))


if __name__ == '__main__':
    main()
