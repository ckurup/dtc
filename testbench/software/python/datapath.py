import numpy as np
from typing import List, Iterable, Generator

HEADER_LINES = 12
SLICE_LENGTH = 56
WORDS_PER_FRAME = 2


def loadFile(filename: str) -> Generator[str, None, None]:
    """
    Load pattern file and convert the first column to a stream of binary words.
    """
    with open(filename) as f:
        for index, line in enumerate(f):
            if index > HEADER_LINES:
                line_stripped: str = line.strip()
                frame: str = line_stripped.split(":")[1].split(" ")[1][2:]
                word: str = "{:0{slice_length}b}".format(
                    int(frame, 16), slice_length=SLICE_LENGTH)[::-1]
                yield word


def combineWords(words: Iterable[str]) -> Generator[str, None, None]:
    """
    Combine 56-bit binary words to 112-bit wide bus frames.
    """
    # Use the L1A line as a fingerprint to detect which half of the wide bus
    # frame is contained withing the first word of the stream. When not sending
    # data, the L1A byte should be 0xAA.
    l1a_slice = (40, 48)
    word_list = [i for i in words]
    if word_list[0][l1a_slice[0]:l1a_slice[1]] == "01010101":
        offset = 0
        frame_count = int(len(word_list)/WORDS_PER_FRAME)
    else:
        offset = 1
        frame_count = int(len(word_list[1:])/WORDS_PER_FRAME)
    gbt_frames: List[str] = []
    for i in range(frame_count):
        yield word_list[i*WORDS_PER_FRAME + offset] + word_list[i*WORDS_PER_FRAME + offset + 1]


def generateGBTFrames(filename: str) -> Generator[str, None, None]:
    """
    Load pattern file nad combine alternate words to generate wide bus frames.
    """
    return combineWords(loadFile(filename))
