import numpy as np
from typing import List

LINK_INDICES = ((96, 32, 80, 8, 88), (56, 104, 24, 72, 16))


def frame(widebus_word):
    cic_frames = []
    for i, indices in enumerate(LINK_INDICES):
        bx = np.array([np.array(list(widebus_word[index:index+8]), dtype=int) for index in indices]).T
        cic_frames.append(np.flip(bx, 0))
    return cic_frames
