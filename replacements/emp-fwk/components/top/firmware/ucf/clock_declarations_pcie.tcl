
# 100 Mhz PCIe system clock
create_clock -period 10.000 -name pcie_sys_clk [get_ports pcie_sys_clk_p]

# IPbus clock
create_generated_clock -name ipbus_clk -source [get_pins infra/clocks/mmcm/CLKIN1] [get_pins infra/clocks/mmcm/CLKOUT1]

# AXI clock
create_generated_clock -name axi_clk [get_pins -hierarchical -filter {NAME =~infra/dma/xdma/*/phy_clk_i/bufg_gt_userclk/O}]

# Approx 40MHz clock derived from AXI clock (for tests without external clock source)
#create_generated_clock -name clk_40_pseudo_i [get_pins infra/clocks/mmcm/CLKOUT2]
