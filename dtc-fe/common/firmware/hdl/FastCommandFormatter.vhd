library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.ipbus.all;
use work.ipbus_reg_types.all;

use work.emp_data_types.all;

use work.gbt_module_constants.all;
use work.front_end_data_types.all;


-- eventually we will need clk40 from emp-ttc
-- until then strobe_40mhz used to same effect in clk_p domain

entity FastCommandFormatter is
port(
    --- Input Ports ---
    clk               : in std_logic;
    cmd_global        : in tFastCommand;
    fast_cmd_ctrl     : in ipb_reg_v(3 downto 0);
    --- Output Ports ---
    data_out          : out lword := LWORD_NULL;
    fast_cmd_status   : out ipb_reg_v(0 downto 0)
);
end FastCommandFormatter;


architecture rtl of FastCommandFormatter is


constant PIPE_WIDTH             : integer := 8;

signal strobe_40mhz             : std_logic;
signal local_bc0                : std_logic;


-- IPBus registers

signal source_select            : std_logic := '0';
signal tap_select               : integer range 0 to PIPE_WIDTH - 1 := 0;
signal local_gen_run            : std_logic := '0';
signal local_gen_bc0_disable    : std_logic := '0';
signal local_gen_repeat         : integer range 0 to 1023 := 0;

type tFastCmdBuffer is array(6 downto 0) of std_logic_vector(15 downto 0);
signal local_gen_buffer         : tFastCmdBuffer := (others => (others => '0'));


-- Local fast command generator

type tGenState is (start, countdown, stop);
signal local_gen                : tGenState := start;

signal lgen_instruction         : std_logic_vector(1 downto 0) := (others => '0');
signal lgen_counter             : integer range 0 to 1023 := 0;
signal lgen_rpt_counter         : integer range 0 to 1023 := 0;
signal cmd_gen                  : tFastCommand;

signal ctr_reset_counter        : integer range 0 to 255 := 0;
signal cal_pulse_counter        : integer range 0 to 255 := 0;
signal l1a_trig_counter         : integer range 0 to 255 := 0;
signal fast_reset_counter       : integer range 0 to 255 := 0;


-- Output delay & formatting

signal cmd_local                : tFastCommand;
signal cmd_out                  : tFastCommand;

type tCmdPipe is array(PIPE_WIDTH - 1 downto 0) of tFastCommand;
signal cmd_pipe                 : tCmdPipe := (others => (others => '0'));


begin



--==============================--
-- Local fast command generator
--==============================--


--==============================--
fsm: process(clk)
--==============================--

    variable buf_index: integer range 0 to 6 := 0;

begin

    if rising_edge(clk) and strobe_40mhz = '1' then

        -- latch IPBus control registers in payload clock domain
        source_select <= fast_cmd_ctrl(0)(0);
        tap_select    <= to_integer(unsigned(fast_cmd_ctrl(0)(3 downto 1)));

        local_gen_run         <= fast_cmd_ctrl(0)(4);
        local_gen_bc0_disable <= fast_cmd_ctrl(0)(5);
        local_gen_repeat      <= to_integer(unsigned(fast_cmd_ctrl(0)(15 downto 6)));

        -- 16b fast command generator commands : instruction [2b] + wait counter [10b] + fast command [4b]
        -- instructions : 0 = return to first word; 1 = move to next word; 2 = stop at this word; 3 = undefined
        -- fast commands: bit3 = fast_reset; bit2 = l1a_trigger; bit1 = cal_pulse; bit0 = counter_reset
        local_gen_buffer(0) <= fast_cmd_ctrl(1)(15 downto 0);
        local_gen_buffer(1) <= fast_cmd_ctrl(1)(31 downto 16);
        local_gen_buffer(2) <= fast_cmd_ctrl(2)(15 downto 0);
        local_gen_buffer(3) <= fast_cmd_ctrl(2)(31 downto 16);
        local_gen_buffer(4) <= fast_cmd_ctrl(3)(15 downto 0);
        local_gen_buffer(5) <= fast_cmd_ctrl(3)(31 downto 16);
        local_gen_buffer(6) <= (others => '0');   --null buffer

        -- only run fast command generator when local_gen_run enabled
        if local_gen_run = '1' then

            case local_gen is

                -- initial state when local_gen_run enabled; initialise and send fast command in first word
                when start =>
                   buf_index        := 0;

                   lgen_counter     <= to_integer(unsigned(local_gen_buffer(buf_index)(13 downto 4)));
                   lgen_instruction <= local_gen_buffer(buf_index)(15 downto 14);

                   cmd_gen.fast_reset    <= local_gen_buffer(buf_index)(3);
                   cmd_gen.l1a_trig      <= local_gen_buffer(buf_index)(2);
                   cmd_gen.cal_pulse     <= local_gen_buffer(buf_index)(1);
                   cmd_gen.counter_reset <= local_gen_buffer(buf_index)(0);

                   local_gen <= countdown;

                -- wait for N clk cycles as defined in current word; then decide on next action
                when countdown =>
                    if lgen_counter <= 0 then
                        if lgen_instruction = "01" then
                            buf_index := buf_index + 1;  -- move to next word
                        elsif lgen_instruction = "00" and lgen_rpt_counter > 1 then
                            lgen_rpt_counter <= lgen_rpt_counter - 1;  -- decrement repeat sequence counter
                            buf_index := 0;  -- return to word 0
                        elsif lgen_instruction = "00" and lgen_rpt_counter = 0 then
                            buf_index := 0;  -- return to word 0 and repeat ad infinitum
                        else
                            local_gen <= stop;
                            buf_index := 6;  -- instruction indicates stop or is undefined
                        end if;

                        -- set counter, instruction, and fast commands according to next word
                        lgen_counter     <= to_integer(unsigned(local_gen_buffer(buf_index)(13 downto 4)));
                        lgen_instruction <= local_gen_buffer(buf_index)(15 downto 14); 

                        cmd_gen.fast_reset    <= local_gen_buffer(buf_index)(3);
                        cmd_gen.l1a_trig      <= local_gen_buffer(buf_index)(2);
                        cmd_gen.cal_pulse     <= local_gen_buffer(buf_index)(1);
                        cmd_gen.counter_reset <= local_gen_buffer(buf_index)(0);
                    else
                        lgen_counter <= lgen_counter - 1;
                        cmd_gen      <= ('0', '0', '0', '0');
                    end if;

                -- final state when buffers end, instruction indicates stop, or undefined sequence
                -- remain here until local_gen_run is reset
                when others =>
                    cmd_gen <= ('0', '0', '0', '0');

            end case;

            -- update local gen fast counters for debugging when local_gen_run is enabled
            if (fast_reset_counter < 255 and cmd_gen.fast_reset = '1') then
                fast_reset_counter <= fast_reset_counter + 1;
            end if;

            if (l1a_trig_counter < 255 and cmd_gen.l1a_trig = '1') then
                l1a_trig_counter <= l1a_trig_counter + 1;
            end if;

            if (cal_pulse_counter < 255 and cmd_gen.cal_pulse = '1') then
                cal_pulse_counter <= cal_pulse_counter + 1;
            end if;

            if (ctr_reset_counter < 255 and cmd_gen.counter_reset = '1') then
                ctr_reset_counter <= ctr_reset_counter + 1;
            end if;

        else

            -- reset fsm and counters when local_gen_run is disabled
            local_gen          <= start;
            lgen_rpt_counter   <= local_gen_repeat;
            cmd_gen            <= ('0', '0', '0', '0');
            fast_reset_counter <= 0;
            l1a_trig_counter   <= 0;
            cal_pulse_counter  <= 0;
            ctr_reset_counter  <= 0;

        end if;

    end if;

end process fsm;

-- prepare locally generated fast commands (OR with cyclic BC0 unless disabled)
cmd_local.fast_reset    <= cmd_gen.fast_reset;
cmd_local.l1a_trig      <= cmd_gen.l1a_trig;
cmd_local.cal_pulse     <= cmd_gen.cal_pulse;
cmd_local.counter_reset <= cmd_gen.counter_reset when local_gen_bc0_disable = '1' else cmd_gen.counter_reset or local_bc0;

-- prepare IPBus status registers
fast_cmd_status(0)(7 downto 0)   <= std_logic_vector(to_unsigned(ctr_reset_counter, 8));
fast_cmd_status(0)(15 downto 8)  <= std_logic_vector(to_unsigned(cal_pulse_counter, 8));
fast_cmd_status(0)(23 downto 16) <= std_logic_vector(to_unsigned(l1a_trig_counter, 8));
fast_cmd_status(0)(31 downto 24) <= std_logic_vector(to_unsigned(fast_reset_counter, 8));



--==============================--
-- Output : source select, coarse delay & formatting
--==============================--


--==============================--
delay: process(clk)
--==============================--
begin

    if rising_edge(clk) and strobe_40mhz = '1' then

        -- select between global and local fast command sources
        if source_select = '0' then
            cmd_pipe(0) <= cmd_global;
        else
            cmd_pipe(0) <= cmd_local;
        end if;

        -- delay commands in 25ns increments using a shift register
        cmd_pipe(PIPE_WIDTH - 1 downto 1) <= cmd_pipe(PIPE_WIDTH - 2 downto 0);

    end if;

end process delay;


--==============================--
to_clkp: process(clk)
--==============================--
begin

    if rising_edge(clk) and strobe_40mhz = '1' then

        -- latch delayed commands into clk_p domain and hold
        cmd_out <= cmd_pipe(tap_select);

    end if;

end process to_clkp;


--==============================--
genOutput: for i in 0 to 1 generate
--==============================--
begin

    data_out.data(16*i + 15 downto 16*i) <= fastCommandToSLV(cmd_out);

end generate genOutput;


data_out.valid  <= '1';
data_out.strobe <= '1';



--==============================--
-- Strobe generators
--==============================--


--==============================--
bx_strobe: process(clk)
--==============================--

    variable timer : natural range 0 to 7 := 7;

begin

    if rising_edge(clk) then

        if timer = 0 then
            timer := 7;
            strobe_40mhz <= '1';
        else
            timer := timer - 1;
            strobe_40mhz <= '0';
        end if;

    end if;

end process bx_strobe;


--==============================--
bc0_strobe: process(clk)
--==============================--

    variable timer : natural range 0 to 3563 := 3563;

begin

    if rising_edge(clk) and strobe_40mhz = '1' then

        if timer = 0 then
            timer := 3563;
            local_bc0 <= '1';
        else
            timer := timer - 1;
            local_bc0 <= '0';
        end if;

    end if;

end process bc0_strobe;



end rtl;
