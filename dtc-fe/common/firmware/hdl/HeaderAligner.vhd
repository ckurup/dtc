----
-- Author: David Monk
-- Description file for HeaderAligner entity
-- This entity reads in the same stream as the StubExtractor and locates the
-- start of the boxcar. This information is then sent to the StubExtractor entity
-- to extract the stubs.
----


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.emp_data_types.all;

entity HeaderAligner is
    port (
        --- Input Ports ---
        clk     : in std_logic;
        data_in : in lword     := LWORD_NULL;
        reset   : in std_logic := '0';
        --- Output Ports ---
        header_start : out std_logic                    := '0';
        state        : out std_logic_vector(2 downto 0) := (others => '0');
        --- Debug Ports ---
        debug_header_match : out std_logic;
        debug_header_mismatch_count : out std_logic_vector(31 downto 0)
    );
end HeaderAligner;


architecture Behavorial of HeaderAligner is

    signal data_reg              : lword                := LWORD_NULL;

    signal header_match          : std_logic            := '0';
    signal header_start_internal : std_logic            := '0';
    signal counter               : unsigned(5 downto 0) := (others => '0');

    constant cHeaderSignalDelay : integer := 56;

    type aligner_state is (unlocked, locking, locked);
    signal aligner : aligner_state := unlocked;

    signal sr_header_start : std_logic_vector(5 downto 0) := (others => '0');
    signal header_mismatch_count : unsigned(31 downto 0)  := (others => '0');

begin

    ff: process(clk)
    begin
        if rising_edge(clk) then
            data_reg <= data_in;
        end if;
    end process;


    header_start <= header_start_internal;

    align : process(clk)
    begin
        if rising_edge(clk) then
            if data_reg.strobe = '1' then

                if reset = '1' then
                    aligner <= unlocked;
                end if;

                case aligner is

                when unlocked =>
                    state <= "001";
                    header_start_internal <= '1';

                    if header_match = '1' then
                        counter <= to_unsigned(cHeaderSignalDelay, 6);
                        aligner <= locking;
                    end if;

                when locking =>
                    state <= "010";
                    header_start_internal <= '1';
                    counter <= counter - 1;
                    if counter = 0 then
                        aligner <= locked;
                    end if;

                when locked =>
                    state <= "100";
                    counter <= counter - 1;

                    if counter = 0 then
                        header_start_internal <= '1';
                    else
                        header_start_internal <= '0';
                    end if;

                when others =>
                    header_start_internal <= '1';

                end case;
            else
                case aligner is
                    when locked =>
                        header_start_internal <= '0';
                when others =>
                        header_start_internal <= '1';
                end case;
            end if;
        end if;
    end process;


    --==============================--
    -- Check data for header fingerprint
    --==============================--

    --==============================--
    FingerPrintChecker: entity work.HeaderFingerprintChecker
    --==============================--
    port map (
        --- Input Ports ---
        clk => clk,
        data_in => data_reg,
        --- Output Ports ---
        header_match => header_match
    );


    --==============================--
    -- Debugging
    --==============================--
    debug_header_match <= header_match;

    pHeaderCheck : process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                header_mismatch_count <= (others => '0');
            else
                if aligner = locked then
                    sr_header_start <= sr_header_start(sr_header_start'high - 1 downto sr_header_start'low) & header_start_internal;
                    if sr_header_start(sr_header_start'high) /= header_match then
                        header_mismatch_count <= header_mismatch_count + 1;
                    end if;
                end if;
            end if;
        end if;
    end process;

    debug_header_mismatch_count <= std_logic_vector(header_mismatch_count);

end architecture;
