library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package front_end_data_types is

type tCICHeader is record
    CIC_conf       : std_logic;
    status         : std_logic_vector(8 downto 0);
    bcid           : std_logic_vector(11 downto 0);
    stub_count     : std_logic_vector(5 downto 0);
end record;
type tCICHeaderArray is array(integer range <>) of tCICHeader;

function cicHeaderToSLV (header: tCICHeader) return std_logic_vector;

type tFastCommand is record
    fast_reset     : std_logic;
    l1a_trig       : std_logic;
    cal_pulse      : std_logic;
    counter_reset  : std_logic;
end record;

function fastCommandToSLV (command: tFastCommand) return std_logic_vector;

end package front_end_data_types;


package body front_end_data_types is

function cicHeaderToSLV (header: tCICHeader) return std_logic_vector is
begin
    return header.CIC_conf & header.status & header.bcid & header.stub_count;
end cicHeaderToSLV;

function fastCommandToSLV (command: tFastCommand) return std_logic_vector is
begin
    return "000" & command.fast_reset & "000" & command.l1a_trig & "000" & command.cal_pulse & "000" & command.counter_reset;
end fastCommandToSLV;

end front_end_data_types;
