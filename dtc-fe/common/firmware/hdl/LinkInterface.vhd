library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.ipbus.all;
use work.ipbus_reg_types.all;

use work.emp_data_types.all;

use work.gbt_module_constants.all;
use work.front_end_data_types.all;


entity LinkInterface is
port(
    --- Input Ports ---
    clk_p       : in  std_logic;
    link_in     : in  lword := LWORD_NULL;
    --- Output Ports ---
    link_out    : out lword := LWORD_NULL;
    stub_out    : out lword := LWORD_NULL;
    header_out  : out tCICHeaderArray(cNumberOfCICs - 1 downto 0) := (others => ('0', (others => '0'), (others => '0'), (others => '0')));
    --- IPBus Ports ---
    clk         : in  std_logic;
    rst         : in  std_logic;
    ipb_in      : in  ipb_wbus;
    ipb_out     : out ipb_rbus;
    --- Debug Ports ---
    debug_header_start : out std_logic_vector(1 downto 0);
    debug_header_match : out std_logic_vector(1 downto 0);
    debug_aligner_state : out std_logic_vector(5 downto 0)
);
end LinkInterface;

architecture rtl of LinkInterface is


-- IPBus fabric

signal status_registers         : ipb_reg_v(5 downto 0) := (others => (others => '0'));
signal control_registers        : ipb_reg_v(5 downto 0) := (others => (others => '0'));

signal aligner_status           : ipb_reg_v(0 downto 0) := (others => (others => '0'));
signal aligner_ctrl             : ipb_reg_v(0 downto 0) := (others => (others => '0'));

signal header                   : ipb_reg_v(1 downto 0) := (others => (others => '0'));
signal header_mismatch_count_reg: ipb_reg_v(1 downto 0) := (others => (others => '0'));

signal fast_cmd_status          : ipb_reg_v(0 downto 0) := (others => (others => '0'));
signal fast_cmd_ctrl            : ipb_reg_v(3 downto 0) := (others => (others => '0'));


-- Link decoding and module readout

signal cic_header_array         : tCICHeaderArray(cNumberOfCICs - 1 downto 0) := (others => ('0', (others => '0'), (others => '0'), (others => '0')));

signal stubs                    : ldata(cNumberOfCICs - 1 downto 0) := (others => LWORD_NULL);
signal stub_out_pre_conversion  : lword                             := LWORD_NULL;


-- Link encoding and module control

signal cmd_global               : tFastCommand := ('0', '0', '0', '0');  --connect to global tcds eventually


begin


header_out <= cic_header_array;


--==============================--
-- IPBus fabric
--==============================--


--==============================--
fe_dtc_control: entity work.ipbus_ctrlreg_v
--==============================--
generic map(
    N_CTRL       => 6,
    N_STAT       => 6
)
port map(
    clk          => clk,
    reset        => rst,
    ipbus_in     => ipb_in,
    ipbus_out    => ipb_out,
    d            => status_registers,
    q            => control_registers
);

status_registers(0 downto 0) <= aligner_status;
status_registers(1 downto 1) <= fast_cmd_status;
status_registers(3 downto 2) <= header;
status_registers(5 downto 4) <= header_mismatch_count_reg;


aligner_ctrl                 <= control_registers(0 downto 0);
fast_cmd_ctrl                <= control_registers(4 downto 1);



--==============================--
-- Link decoding and module readout
--==============================--


--==============================--
LinkInterface: for i in 0 to cNumberOfCICs - 1 generate
--==============================--

    signal stream_in         : lword                        := LWORD_NULL;
    signal stream_in_aligned : lword                        := LWORD_NULL;
    signal aligner_reset     : std_logic                    := '0';
    signal aligner_state     : std_logic_vector(2 downto 0) := (others => '0');
    signal header_start      : std_logic                    := '0';

begin

    stream_in.valid                              <= link_in.valid;
    stream_in.strobe                             <= link_in.data(32*i + 31);
    stream_in.data(cNumberOfELinks - 1 downto 0) <= link_in.data(32*i + cNumberOfELinks - 1 downto 32*i);

    aligner_reset                                <= aligner_ctrl(0)(i);
    aligner_status(0)(4*i + 3 downto 4*i)        <= '0' & aligner_state;
    header(i)(27 downto 0)                       <= cicHeaderToSLV(cic_header_array(i));


    -- Debugging
    debug_header_start(i) <= header_start;
    debug_aligner_state(3*(i+1) - 1 downto 3*i) <= aligner_state;

    -- --==============================--
    -- FrameAlignerInstance : entity work.FrameAligner
    -- --==============================--
    -- generic map (
    --     index => i
    -- )
    -- port map (
    --     --- Input Ports ---
    --     clk_p => clk,
    --     data_in => stream_in,
    --     --- Output Ports ---
    --     data_out => stream_in_aligned
    -- );

    --==============================--
    HeaderAlignerInstance: entity work.HeaderAligner
    --==============================--
    port map(
        --- Input Ports ---
        clk              => clk_p,
        data_in          => stream_in,
        reset            => aligner_reset,
        --- Output Ports ---
        header_start     => header_start,
        state            => aligner_state,
        --- Debug Ports ---
        debug_header_match => debug_header_match(i),
        debug_header_mismatch_count => header_mismatch_count_reg(i)
    );

    --==============================--
    StubExtractorInstance: entity work.StubExtractor
    --==============================--
    generic map(
        cic_index        => i
    )
    port map(
        --- Input Ports ---
        clk              => clk_p,
        data_in          => stream_in,
        header_start     => header_start,
        --- Output Ports ---
        stub_out         => stubs(i),
        header_out       => cic_header_array(i)
    );

end generate LinkInterface;


--==============================--
-- Stub interleaving
--==============================--

--==============================--
StubInterleaverInstance: entity work.StubInterleaver
--==============================--
port map(
    --- Input Ports ---
    clk           => clk_p,
    stub_in_0     => stubs(0),
    stub_in_1     => stubs(1),
    --- Output Ports ---
    stub_out      => stub_out
);


--==============================--
-- Stub conversion for input to router
--==============================--

-- --==============================--
-- StubConverterInstance: entity work.StubConverter
-- --==============================--
-- port map(
--     --- Input Ports ---
--     clk           => clk_p,
--     stub_in       => stub_out_pre_conversion,
--     --- Output Ports ---
--     stub_out      => stub_out
-- );



--==============================--
-- Link encoding and module control
--==============================--


--==============================--
FastCommandInstance: entity work.FastCommandFormatter
--==============================--
port map(
    --- Input Ports ---
    clk              => clk_p,
    cmd_global       => cmd_global,
    fast_cmd_ctrl    => fast_cmd_ctrl,
    --- Output Ports ---
    data_out         => link_out,
    fast_cmd_status  => fast_cmd_status
);


end rtl;
