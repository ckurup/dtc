library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.gbt_module_constants.all;

package gbt_framer_data_types is
    type tUnboundedIntegerArray is array(integer range <>) of integer;
    subtype tLinkIndicesArray is tUnboundedIntegerArray(0 to cNumberOfELinks - 1);
    subtype tL1AIndicesArray is tUnboundedIntegerArray(0 to cNumberOfCICs - 1);
    type tUnbounded2DIntegerArray is array(integer range <>) of tLinkIndicesArray;
    subtype tMultiCICLinkIndicesArray is tUnbounded2DIntegerArray(0 to cNumberOfCICs - 1);

    type tUnboundedLinkArray is array(integer range <>) of std_logic_vector(7 downto 0);
    subtype tLinkArray is tUnboundedLinkArray(cNumberOfELinks - 1 downto 0);
    constant NullLinkArray : tLinkArray := (others => (others => '0'));

    type tUnboundedMultiCICLinkArray is array(integer range <>) of tLinkArray;
    subtype tMultiCICLinkArray is tUnboundedMultiCICLinkArray(0 to cNumberOfCICs - 1);
    constant NullMultiCICLinkArray : tMultiCICLinkArray := (others => NullLinkArray);
end package gbt_framer_data_types;
