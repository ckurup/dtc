library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.gbt_framer_data_types.all;

package gbt_framer_constants is
    constant link_indices     : tMultiCICLinkIndicesArray := ((64, 72, 80, 88, 96), (8, 16, 24, 32, 40));
    constant l1a_indices      : tL1AIndicesArray          := (0, 56);
    constant link_offsets     : tMultiCICLinkIndicesArray := ((0, 0, 0, 0, 0), (0, 0, 0, 0, 0));
end package gbt_framer_constants;
