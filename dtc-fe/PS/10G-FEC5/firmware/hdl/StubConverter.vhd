library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.config.all;
use work.emp_data_types.all;

use work.gbt_module_constants.all;
use work.front_end_data_types.all;


entity StubConverter is
port(
    --- Input Ports ---
    clk         : in  std_logic;
    stub_in     : in  lword := LWORD_NULL;
    --- Output Ports ---
    stub_out    : out lword := LWORD_NULL
);
end StubConverter;


architecture Behavorial of StubConverter is

begin
    -- Valid
    stub_out.valid <= stub_in.valid;
    -- Bx
    -- This doesn't do a modulo 72 calculation here, need to clarify how exactly this should be done
    stub_out.data(widthBX - 1 downto 0) <= stub_in.data(widthBx - cModuleStubBxWidth + cHeaderMultiplicityWidth + cStubWidth - 1 downto cHeaderMultiplicityWidth + cStubWidth) & stub_in.data(cModuleStubBxWidth + cModuleRowWidth + cModuleBendWidth - 1 downto cModuleRowWidth + cModuleBendWidth);
    -- Row
    stub_out.data(cModuleRowWidth + widthBx - 1 downto widthBx) <= stub_in.data(cModuleRowWidth + cModuleBendWidth - 1 downto cModuleBendWidth);
    -- Column
    stub_out.data(widthRow + widthBx) <= stub_in.data(46);
    -- Bend
    stub_out.data(cModuleBendWidth + widthCol + widthRow + widthBx - 1 downto widthCol + widthRow + widthBx) <= stub_in.data(cModuleBendWidth - 1 downto 0);
end architecture;
