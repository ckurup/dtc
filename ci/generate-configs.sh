#!/bin/bash


/bin/cat <<EOF > configure-ci.yml
stages:
  - Launch Pipeline
EOF

sed -e '$a\' build_configs.txt | \
while IFS=' ' read -r PIPELINE_NAME PROJECT_ALGORITHM PROJECT_DEPFILE BUILD; do

  if [[ -z "${PIPELINE_NAME}" || ${PIPELINE_NAME} =~ ^/ ]]; then
    continue
  fi

/bin/cat <<EOF >> configure-ci.yml
$PIPELINE_NAME:
  stage: Launch Pipeline
EOF

  if [[ ${BUILD} = "manual" && ${CI_PIPELINE_SOURCE} != "schedule" ]]; then

/bin/cat <<EOF >> configure-ci.yml
  when: manual
EOF

  fi

/bin/cat <<EOF >> configure-ci.yml
  trigger:
    include: ci/.launch_vivado.yml
    strategy: depend
  variables:
    PIPELINE_NAME: ${PIPELINE_NAME}
    PROJECT_ALGORITHM: ${PROJECT_ALGORITHM}
    PROJECT_DEPFILE: ${PROJECT_DEPFILE}
EOF

  if [[ ${CI_PIPELINE_SOURCE} != "schedule" ]]; then

/bin/cat <<EOF >> configure-ci.yml
    EMPHUB_TAG: ${PIPELINE_NAME}_${CI_COMMIT_REF_NAME}-${CI_COMMIT_SHORT_SHA}
EOF

  else

/bin/cat <<EOF >> configure-ci.yml
    EMPHUB_TAG: ${PIPELINE_NAME}_${SCHEDULE_REF_NAME}
EOF

  fi

done
